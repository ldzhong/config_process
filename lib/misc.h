/* 
 * Copyright (c) 2018 by ultra.gammaray@gmail.com - All rights reserved.  
 */

#ifndef _MISC_H_
#define _MISC_H_

int path_is_absolute(const char *path);

#endif
