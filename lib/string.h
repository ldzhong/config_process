/* 
 * Copyright (c) 2018 by ultra.gammaray@gmail.com - All rights reserved.  
 */

#ifndef _STRING_H_
#define _STRING_H_

char *strdup_vprintf(const char *format, va_list ap);
char *strdup_printf(const char *format, ...);
char *strskp(char *s);
char * strlwc(char * s, char * l);
char * strcrop(char * s, char * l);

#endif
