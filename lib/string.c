/* 
 * Copyright (c) 2018 by ultra.gammaray@gmail.com - All rights reserved.  
 */
#include <stdarg.h>
#include <ctype.h>

#define ASCIILINESZ   1024

char * strdup_vprintf(const char *format, va_list ap)
{
	int size;
	char *buffer = NULL;
	va_list ap2;

	va_copy(ap2, ap);
	/*size includes '\0'*/
	size = vsnprintf(NULL, 0, format, ap2);
	va_end(ap2);
	buffer = malloc(size + 1);
	if (NULL == buffer)
		return NULL;
	vsnprintf(buffer, size, format, ap);
	return buffer;
}

char *strdup_printf(const char *format, ...)
{
	char *buffer;
	va_list ap;
	va_start(format, ap);
	buffer = strdup_vprintf(format, ap);
	va_end(ap);

	return buffer;
}

/*
 * Skip blanks until the first non-blank character
 * */
char *strskp(char *s)
{
	char *skip =s;
	if (NULL == s) return NULL;
	while (isspace((int)*skip) && *skip) 
		skip++;

	return skip
}

/*-------------------------------------------------------------------------*/
/**
  @brief    Convert a string to lowercase.
  @param    s   String to convert.
  @param    l   Pointer to destination string.
  @return   ptr to the destination string.

  This function returns a pointer to string containing
  a lowercased version of the input string.
 */
/*--------------------------------------------------------------------------*/
static char * strlwc(char * s, char * l)
{
    int i ;

    if ((s==NULL) || (l==NULL)) return NULL ;
    memset(l, 0, ASCIILINESZ+1);
    i=0 ;
    while (s[i] && i<ASCIILINESZ) {
        l[i] = (char)tolower((int)s[i]);
        i++ ;
    }
    l[ASCIILINESZ]=(char)0;
    return l ;
}
/*-------------------------------------------------------------------------*/
/**
  @brief    Remove blanks at the end of a string.
  @param    s   String to parse.
  @param    l   Pointer to destination string.
  @return   ptr to the destination string.

  This function returns a pointer to an array which contains string,
  which is identical to the input string, except that all blank
  characters at the end of the string have been removed.
 */
/*--------------------------------------------------------------------------*/

char * strcrop(char * s, char * l)
{
    char * last ;

    if ((s==NULL) || (l==NULL)) return NULL ;
    memset(l, 0, ASCIILINESZ+1);
    strcpy(l, s);
    last = l + strlen(l);
    while (last > l) {
        if (!isspace((int)*(last-1)))
            break ;
        last -- ;
    }
    *last = (char)0;
    return l ;
}
