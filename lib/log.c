/* 
 * refer to lvm2/lib/log/log.c
 * Copyright (c) 2018 by ultra.gammaray@gmail.com - All rights reserved.  
 */
#include "log.h"
#include <stdarg.h>
#include <limits.h>

static int  _log_level         = _LOG_WARN;
static bool _syslog_enabled    = false;
static bool _log_file_enabled   = false;
static FILE *_log_file          = NULL;
static char _log_file_path[PATH_MAX];

bool is_syslog_enabled()
{
	return _syslog_enabled;
}

void syslog_enable()
{
	_syslog_enabled = true;
}

void log_file_enable(const char *log_file, int append)
{
	_log_file_enabled = true;
	if (!(_log_file = fopen(log_file, append?"a":"w"))) {
		printf("%s-%d: fopen failed\n", __FILE__, __LINE__);
		exit(-1);
	}
}

static int log_fclose(FILE *stream)
{
	int prev_fail = ferror(stream);
	int fclose_fail = fclose(stream);

	/* If there was a previous failure, but fclose succeeded,
	   clear errno, since ferror does not set it, and its value
	   may be unrelated to the ferror-reported failure.  */
	if (prev_fail && !fclose_fail)
		errno = 0;

	return prev_fail || fclose_fail ? EOF : 0;
}

void log_file_disable()
{
	_log_file_enabled = false;
	if (log_fclose(_log_file)) {
		if (errno) 
			fprintf(stderr, "failed to close log file %s\n", strerror(errno));
		else
			fprintf(stderr, "failed to close log file\n");
	}
}

void log_set_level(int level)
{
	_log_level = level;
}

int log_get_level()
{
	return _log_level;
}

__attribute__((format(printf,5,0)))
static void _vprint_log(int level, char *file, int line, int error, va_list vl)
{
	va_list ap;
	
	if(_log_file_enabled) {
	}
}

void print_log(int level, char *file, int line, int error, const char *format, ...)
{
	va_list vl;

	va_start(vl, format);
	_vprint_log(level, file, line, error, vl);
	va_end(vl);
}
