/* 
 * Copyright (c) 2018 by ultra.gammaray@gmail.com - All rights reserved.  
 */

int path_is_absolute(const char *path)
{
	int ret = 0;

#ifdef WIN32
	if (':' == path[1])
		ret = 1;
#endif

	// will count as absolute path if starting with '.' or '/'
	if (('.' == path[0]) || ('/' == path[0]))
		ret = 1;
	return ret;
}
