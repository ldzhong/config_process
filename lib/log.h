/* 
 * Copyright (c) 2018 by ultra.gammaray@gmail.com - All rights reserved.  
 */

#ifndef _LOG_H_
#define _LOG_H
#include <syslog.h>

enum {
	_LOG_FATAL,
	_LOG_ERR,
	_LOG_WARN,
	_LOG_NOTICE,
	_LOG_INFO,
	_LOG_DEBUG
};

#define EUNCLASSIFIED -1  /*generic error code*/

#define log_debug(x...)   LOG_LINE(_LOG_DEBUG, x)
#define log_info(x...)    LOG_LINE(_LOG_INFO, x)
#define log_notice(x...)  LOG_LINE(_LOG_NOTICE, x)
#define log_warn(x...)    LOG_LINE(_LOG_WARN, x)
#define log_err(x...)     LOG_LINE_WITH_ERRNO(_LOG_ERR, ENUCLASSIFIED, x)
#define log_fatal(x...)   LOG_LINE_WITH_ERRNO(_LOG_FATAL, EUNCLASSIFIED, x)

#define LOG_LINE(l, x...)  \
	print_log(l, __FILE__, __LINE__, 0, ## x)
#define LOG_LINE_WITH_ERRNO(l, e, x...)   \
	print_log(l, __FILE__, __LINE__, e, ## x)

void log_set_level(int level);
int log_get_level();
void syslog_enable();
void log_file_enable();
bool is_syslog_enable();
void print_log(int level, char *file, int line, int error, const char *format, ...);


#endif
